#!/bin/bash
rm -rf "$BB_ORG" && mkdir "$BB_ORG" && cd $BB_ORG

URL="https://api.bitbucket.org/2.0/repositories/$BB_ORG?pagelen=100"


curl -u $BB_USERNAME:$BB_PASSWORD $URL > repoinfo.json
jq -r '.values[] | .links.clone[0].href' repoinfo.json > ../repos.txt
URL=`jq -r '.next' repoinfo.json`
for repo in `cat ../repos.txt`
    do
    echo
    echo "* Processing $repo..."
    echo
    git clone --bare $repo 
    fullName="$(cut -d '/' -f5 <<<"$repo")"
    repoName="$(cut -d '.' -f1 <<<"$fullName")"
    echo $repoName
    echo "* $repoName cloned, now creating on github..."  
    echo
    cd $repoName.git
    curl -u $GH_USERNAME:$GH_PASSWORD https://api.github.com/orgs/$GH_ORG/repos -d "{\"name\": \"$repoName\", \"private\": false}"
    echo
    echo "* mirroring $repoName to github..."  
    echo
    git push --mirror $repo && \
    cd ..
done
